var Megaman = Megaman || {};

Megaman.Win = function(game){
    this.game = game;
};

Megaman.Win.prototype = {
    preload: function() {
        this.load.image('win_scene', 'assets/win_scene.png');
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    },
    create: function() {
        this.win_scene = this.add.image(0, 0, 'win_scene');
        this.startButton = this.input.keyboard.addKey(Phaser.Keyboard.ENTER);
    },

    update: function(){       
        if(this.startButton.isDown){
            this.state.start('Menu');
        }
    }
};